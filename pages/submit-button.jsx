import React from "react";

export default class SubmitButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled: true
    };
  }

  componentDidMount() {
    addEventListener("MySuperForm.validate", event => {
      this.setState({ disabled: !event.detail });
    });
  }

  render() {
    return (
      <button disabled={this.state.disabled} type="submit">
        Send!
      </button>
    );
  }
}
