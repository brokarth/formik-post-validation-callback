import React from "react";

const EmailField = ({ field, form }) => (
  <div>
    <label>
      Fill Email:
      <input name={field.name} type="email" onChange={field.onChange} onBlur={field.onBlur} value={field.value} />
    </label>
    {form.touched[field.name] && form.errors[field.name] && <span>{form.errors[field.name]}</span>}
  </div>
);

EmailField.validate = email => {
  let error;
  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!email) {
    error = "Please fill the email";
  } else if (!emailRegex.test(email)) {
    error = "Invalid Email";
  }

  return error;
};

export default EmailField;
