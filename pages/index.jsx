import React from "react";

import MySuperForm from "./my-super-form";
import SubmitButton from "./submit-button";

export default () => (
  <main>
    <MySuperForm />
    <SubmitButton />
  </main>
);
