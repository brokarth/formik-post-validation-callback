import React from "react";

const NameField = ({ field, form }) => {
  return (
    <div>
      <label>
        Fill Name:
        <input name={field.name} onChange={field.onChange} onBlur={field.onBlur} value={field.value} />
      </label>
      {form.touched[field.name] && form.errors[field.name] && <span>{form.errors[field.name]}</span>}
    </div>
  );
};

NameField.validate = name => {
  let error;
  if (!name) {
    error = "Please fill the name";
  }

  return error;
};

export default NameField;
