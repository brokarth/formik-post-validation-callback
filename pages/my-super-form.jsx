import React from "react";
import { Formik, Field } from "formik";

import EmailField from "./email-field";
import NameField from "./name-field";

export default class MySuperForm extends React.Component {
  constructor(props) {
    super(props);

    this.initialValues = {
      myEmail: "",
      nameField: ""
    };
  }

  renderFields = () => {
    return (
      <section>
        <Field name="myEmail" component={EmailField} validate={EmailField.validate} />
        <Field name="nameField" component={NameField} validate={NameField.validate} />
      </section>
    );
  };

  renderForm = () => {
    this.checkFormValidity();
    return <form>{this.renderFields()}</form>;
  };

  checkFormValidity() {
    if (this.formik === undefined) {
      return;
    }

    const formikContext = this.formik.getFormikContext();

    const dirty = formikContext.dirty;
    if (!dirty) {
      return;
    }

    const isSubmitting = formikContext.isSubmitting;
    const isValid = formikContext.isValid;

    dispatchEvent(
      new CustomEvent("MySuperForm.validate", {
        detail: !isSubmitting && dirty && isValid
      })
    );
  }

  render() {
    return (
      <Formik
        ref={formik => {
          this.formik = formik;
        }}
        initialValues={this.initialValues}
        render={this.renderForm}
      />
    );
  }
}
