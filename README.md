This project is to replicate the react's warning about updating the sgtate during the rendering
![React warnings](react-warning.png "React warnings")

# To replicate

Start the application with:

```shell
npm run dev
```

Go to [localhost:3000](http://localhost:3000/)

Then type something on any of the fields, the warning should appear inmediatly on the console.
